package com.android.demo.controller;

import com.android.demo.domain.Cart;
import com.android.demo.repository.CartRepository;
import com.android.demo.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/android/cart/")
public class CartController {

    @Autowired
    CartService cartService;
    @Autowired
    CartRepository cartRepository;

    @PostMapping("addCart")
    public ResponseEntity<Cart> addCart(@RequestBody Cart cart){
        return ResponseEntity.ok(cartService.createCart(cart));
    }
    @GetMapping("getCart")
    public ResponseEntity<?> addCart1(){
        return ResponseEntity.ok(cartRepository.findFirstByStatus(0));
    }
}
