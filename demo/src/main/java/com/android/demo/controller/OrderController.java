package com.android.demo.controller;

import com.android.demo.domain.Order;
import com.android.demo.repository.OrderRepository;
import com.android.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/android/order/")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("addOrder")
    public ResponseEntity<Order> addOrder(@RequestBody Order order){
        return ResponseEntity.ok(orderService.createOrder(order));
    }
}
