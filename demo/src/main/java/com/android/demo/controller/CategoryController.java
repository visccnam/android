package com.android.demo.controller;

import com.android.demo.domain.Category;
import com.android.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/android/category/")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("getAllCategory")
    public ResponseEntity<List<Category>> getAllCategory(){
        return ResponseEntity.ok(categoryService.getallCategory());
    }
}
