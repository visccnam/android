package com.android.demo.controller;


import com.android.demo.domain.Product;
import com.android.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/android/product/")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("getAllProduct")
    public ResponseEntity<List<Product>> getAllProduct(){
        return ResponseEntity.ok(productService.getAllProduct());
    }
    @GetMapping("getAllProductByCategory")
    public ResponseEntity<List<Product>> getAllProductByCategory(@RequestParam Integer id){
        return ResponseEntity.ok(productService.getAllProductByCategory(id));
    }
    @GetMapping("getProductById")
    public ResponseEntity<Product> getAllProductById(@RequestParam Integer id){
        return ResponseEntity.ok(productService.getProductById(id));
    }
}
