package com.android.demo.controller;

import com.android.demo.domain.Customer;
import com.android.demo.dto.LoginDTO;
import com.android.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/android/customer/")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("login")
    public ResponseEntity<Customer> checkLogin(@RequestParam String username){
        return ResponseEntity.ok(customerService.checklogin(username));
    }
    @PostMapping("register")
    public ResponseEntity<Customer> checkLogin(@RequestBody Customer customer){
        return ResponseEntity.ok(customerService.register(customer));
    }
}
