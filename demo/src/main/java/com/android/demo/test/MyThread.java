package com.android.demo.test;

public class MyThread extends Thread{
    Count count = new Count();

    public MyThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 1;i<=30;i++){
                    synchronized (count){
                        count.in();
                    }
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread((new Runnable() {
            @Override
            public void run() {
                for(int i=1;i<=30;i++){
                    synchronized (count){
                        count.out();
                    }
                }
            }
        })).start();
    }
}
