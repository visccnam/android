package com.android.demo.test;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class TestThreadPool {
    public static void main(String[] args) {
//        int corePoolSize = 5;
//        int maximumPoolSize = 10;
//        long keepAliveTime = 500;
//        TimeUnit unit = TimeUnit.SECONDS;
//
//        ArrayBlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(100);
//
//        RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy();
//
//        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize,
//                maximumPoolSize, keepAliveTime, unit, workQueue, handler);
//        for (int i = 0; i < 10; i++) {
//            threadPoolExecutor.execute(new Run(i));
//        }

        ExecutorService executorService = Executors.newFixedThreadPool(2);


        Set<Callable<String>> callables = new HashSet<Callable<String>>();

        callables.add(new Callable<String>() {
            public String call()  {
                System.out.println("task 1");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return "Task 1";
            }
        });
        callables.add(new Callable<String>() {
            public String call() throws InterruptedException {
                System.out.println("task 2");
                Thread.sleep(5000);
                return "Task 2";
            }
        });
        callables.add(new Callable<String>() {
            public String call(){
                System.out.println("task 3");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return "Task 3";
            }
        });
        callables.add(new Callable<String>() {
            public String call() throws InterruptedException {
                System.out.println("task 4");
                Thread.sleep(5000);
                return "Task 4";
            }
        });
        callables.add(new Callable<String>() {
            public String call() throws InterruptedException {
                System.out.println("task 5");
                Thread.sleep(5000);
                return "Task 5";
            }
        });

        String result = null;
        try {
            result = executorService.invokeAny(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("result = " + result);

        executorService.shutdown();
    }
}
