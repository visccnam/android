package com.android.demo.repository;

import com.android.demo.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart findFirstByStatus(Integer status);
}
