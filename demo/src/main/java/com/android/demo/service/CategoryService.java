package com.android.demo.service;

import com.android.demo.domain.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getallCategory();
}
