package com.android.demo.service.impl;

import com.android.demo.domain.Product;
import com.android.demo.repository.ProductRepository;
import com.android.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }
    @Override
    public List<Product> getAllProductByCategory(Integer categoryId) {
        return productRepository.findAllByCategoryId(categoryId);
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getById(id);
    }
}
