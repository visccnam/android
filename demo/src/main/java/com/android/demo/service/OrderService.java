package com.android.demo.service;

import com.android.demo.domain.Order;

public interface OrderService {
    Order createOrder(Order order);
}
