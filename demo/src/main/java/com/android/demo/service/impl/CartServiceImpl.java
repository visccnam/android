package com.android.demo.service.impl;

import com.android.demo.domain.Cart;
import com.android.demo.repository.CartRepository;
import com.android.demo.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    CartRepository cartRepository;

    @Override
    public Cart createCart(Cart cart) {

        cart.setStatus(0);
        return cartRepository.save(cart);
    }
}
