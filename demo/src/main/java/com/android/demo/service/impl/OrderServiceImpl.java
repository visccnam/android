package com.android.demo.service.impl;

import com.android.demo.domain.Order;
import com.android.demo.repository.OrderRepository;
import com.android.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Override
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }
}
