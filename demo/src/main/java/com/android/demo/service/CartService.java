package com.android.demo.service;

import com.android.demo.domain.Cart;

public interface CartService {
    Cart createCart(Cart cart);
}
