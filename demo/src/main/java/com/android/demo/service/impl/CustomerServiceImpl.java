package com.android.demo.service.impl;

import com.android.demo.domain.Customer;
import com.android.demo.dto.LoginDTO;
import com.android.demo.repository.CustomerRepository;
import com.android.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;
    @Override
    public Customer checklogin(String username) {
        return customerRepository.findByUsername(username);
    }

    @Override
    public Customer register(Customer customer) {
        return customerRepository.save(customer);
    }
}
