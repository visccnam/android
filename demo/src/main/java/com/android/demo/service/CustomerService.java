package com.android.demo.service;

import com.android.demo.domain.Customer;
import com.android.demo.dto.LoginDTO;

public interface CustomerService {
    Customer checklogin(String username);
    Customer register(Customer customer);

}
