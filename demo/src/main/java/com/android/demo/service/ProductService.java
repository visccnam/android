package com.android.demo.service;

import com.android.demo.domain.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    List<Product> getAllProductByCategory(Integer categoryId);
    Product getProductById(Integer id);
}
