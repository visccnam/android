package com.android.demo.service.impl;

import com.android.demo.domain.Category;
import com.android.demo.repository.CategoryRepository;
import com.android.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategpryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;


    @Override
    public List<Category> getallCategory() {
        return categoryRepository.findAll();
    }
}
